var express= require('express');
var app= express();
var bodyParser= require('body-parser') ;
var mongoose= require('mongoose');
const fetch= require('node-fetch');
var parseString= require('xml2js').parseString;
var async = require('async');

function Game(obj) {   //papildyti
	var res=obj.results;
    return {
        id: res.id,
        name: res.name,
        description: res.description,
        deck: res.deck,
        platforms: res.platforms,
        original_game_rating: res.original_game_rating,
        date_added: res.date_added,
        date_last_updated: res.date_last_updated
    };

}

app.use(bodyParser.json());

Games= require('./models/game');

//connect to mongoose
mongoose.connect('mongodb://localhost/giantbomb');
var db= mongoose.connection;

app.get('/', function(req, res){
	res.send('Pls use /api');
});

app.get('/api/games', function(req, res){
	Games.getGames(function(err, games){
		if(err){
			console.error(err);//kitus errorus pakeis taip
			res.status(500).end();
			return;
		}
		res.json(games);
	});
});



app.get('/api/games/:_id', function(req, res){//perkeleti atskrai

	Games.getGameById(req.params._id, function(err, game)
	{
		if(err)
		{
			var id= req.params._id;//test with 3030-1
			//* If no game is found we fecth it from online source.
			fetch('https://www.giantbomb.com/api/game/'+id+'/?api_key=a620e02484c7a7f773ebfc174a1a68489bfcb7d3&format=json&field_list=id,name,description,deck,platforms,original_game_rating,date_added,date_last_updated')//https://www.giantbomb.com/api/game/3030-1/?api_key=a620e02484c7a7f773ebfc174a1a68489bfcb7d3&format=json&field_list=name,description,deck,platforms,original_game_rating,date_added,date_last_updated')
			 .then(res =>  res.json())
			 .then(data => {
			 	 console.log(data);

			 	res.json(Game(data));
			 })
			 .catch(err => console.error(err));
		}
		else 
		{
			// we return game data from the database
			res.json(game);	
		}
	});
});

app.post('/api/games', function(req, res){
	var game= req.body;
	Games.addGame(game, function(err, game){
		if(err){
			console.error(err);//kitus errorus pakeis taip
			res.status(500).end();
		}
		res.json(game);
	});	
});

app.put('/api/games/:_id', function(req, res){
	var id= req.params._id;
	var game= req.body;
	Games.updateGame(id, game, {}, function(err, game){
		if(err){
			Games.addGame(game, function(err, game){
			if(err){
				console.error(err);//kitus errorus pakeis taip
				res.status(500).end();
			}
		res.json(game);
			});	
		}
		else
		{
		res.json(game);
		}
	});	
});

app.delete('/api/games/:_id', function(req, res){
	var id= req.params._id;
	Games.removeGame(id, function(err, game){
		if(err){
			console.error(err);//kitus errorus pakeis taip
			res.status(500).end();
		}
		res.json(game);
	});	
});

app.get('/api/search/:_query', function(req, res){// when calling use this form: /api/search/word1,word2,...,wordN

	//* If no game is found we fecth it from online source.
	var query= req.params._query;
	query.replace(/,/g,"%");
	//castle%of&resouce=game
			fetch('https://www.giantbomb.com/api/search/?api_key=a620e02484c7a7f773ebfc174a1a68489bfcb7d3&format=json&query='+query+'&resouce=game')//https://www.giantbomb.com/api/game/3030-1/?api_key=a620e02484c7a7f773ebfc174a1a68489bfcb7d3&format=json&field_list=name,description,deck,platforms,original_game_rating,date_added,date_last_updated')
			 .then(res =>  res.json())
			 .then(data => {
			 	 console.log(data);

			 	res.json((data));
			 })
			 .catch(err => console.error(err));
});

app.listen(3000);
console.log('running on port 3000...');