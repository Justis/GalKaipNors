import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './components/Home';
import Search from './components/Search';
import Navbar from './components/CustomBar';


class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Route exact path="/" component={Home} />
          <Route path="/search" component={Search} />
        </div>
      </Router>
      /*<div className="App">
        <header className="App-header">
          <img src={'http://clipartist.net/openclipart.org/2013/Mo/AhNinn/bomb-555px.png'} className="App-logo" alt="logo" />
          <h1 className="App-title">Giant Bomb</h1>
        </header>
        <p className="App-intro">
          <body>
             <br/>SearchField: <input type="text" name="Search"/>
             <button>Click Me!</button><br/>
          </body>
          
        </p>
      </div>*/
    );
  }
}

export default App;
