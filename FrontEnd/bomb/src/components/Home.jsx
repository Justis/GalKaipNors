import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {Jumbotron, Grid, Row, Col, Image, Button} from 'react-bootstrap';
import './Home.css';

export default class Home extends Component{

    constructor(){
        super();
        this.Search = this.Search.bind(this);
        this.state = {
            games: []
        };
    }

    Search(event){
        //event.preventDefault;

        let query = this.refs.searchQuery.value;
        // fetch('http://www.giantbomb.com/api/game/3030-4725/?api_key=a620e02484c7a7f773ebfc174a1a68489bfcb7d3')
        fetch('http://localhost:3000/api/search/castle',{mode: 'no-cors'})//+query)
        .then(response=> console.log(response))
        //.then(results => results.json())
       /* .then(data => {
            JSON.parse(data);
            console.log("state", data);
        })*/
        .catch(error => console.log("Error", error))
    }

    render(){
        return (
           <Grid>
               <Jumbotron>
                <img src={'http://clipartist.net/openclipart.org/2013/Mo/AhNinn/bomb-555px.png'} className="App-logo" alt="logo" />
                <h1 className="App-title">Welcome to Giant Bomb!</h1>
               </Jumbotron>
               <form>
                   <center>
                        <input size="120"
                        ref="searchQuery"
                        type="text"
                        placeholder="Hey mate, what are you looking for?"/>

                        <Button onClick={()=>
                            this.Search()} bsStyle="primary">Search</Button>
                    </center>
               </form>
           </Grid>
        )
    }
}