import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Link} from 'react-dom';
import {Button } from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './components/Home';
import Search from './components/Search';
import Navbar from './components/CustomBar';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Route exact path="/" component={Home} />
          <Route path="/search" component={Search} />
          
        </div>
      </Router>
    );
  }
}

export default App;
