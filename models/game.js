//my API key: 44a0bdbf18fa485e53fcc45b1727cbdc25bfdca7  
// games: https://www.giantbomb.com/api/games/?api_key=44a0bdbf18fa485e53fcc45b1727cbdc25bfdca7
//example game: https://www.giantbomb.com/api/game/3030-1/?api_key=44a0bdbf18fa485e53fcc45b1727cbdc25bfdca7
var mongoose= require('mongoose');
var api_key= '44a0bdbf18fa485e53fcc45b1727cbdc25bfdca7';

// Game Schema
var gameSchema= mongoose.Schema({
	/*id:{
		type: Number,
		required: true 
	},*/
	date_added:{
		type: String,
		//default: Date.now
	},
	date_last_updated:{
		type: String,
		//default: Date.now
	},
	description:{
		type: String
	},
	deck:{
		type: String
	},
	name:{
		type: String,
		required: true
	},
	platforms:{
		type: String
	},
	original_game_rating:{
		type: Number
	}
});

var Game= module.exports= mongoose.model('Game', gameSchema );

module.exports.getGames= function(callback, limit){
	Game.find(callback).limit(limit);
}

module.exports.gg= function(callback, limit){
	Game.find(callback).limit(limit);
}

module.exports.getGameById= function(id, callback){
	Game.findById(id, callback);
}

module.exports.addGame= function(game, callback){
	Game.create(game, callback);
}

module.exports.updateGame= function(id, game, options, callback){
	var query={_id: id};
	var update={
		date_added: game.date_added,
		date_last_updated: game.date_last_updated,
		description:  game.description,
		deck:  game.deck,
		name: game.name,
		platforms:  game.platforms,
		original_game_rating:  game.original_game_rating
	}
	Game.findOneAndUpdate(query, update, options, callback);
}

module.exports.removeGame= function(id, callback){
	var query={_id: id};
	Game.remove(query, callback);
}